package alaposztalyok;

public class PalyazatSport extends Palyazat{

	private int merkozesekSzama;
	private static int MERKOZESENKENTI_DIJ;

	public PalyazatSport(Palyazo palyazo) {
		super(palyazo);
		// TODO Auto-gene��rated constructor stub

	}

	@Override
	public int folyosithatoOsszeg() {
		if(getPalyazo().isEdzoiAjanlas()){
			int osszeg=MERKOZESENKENTI_DIJ*getPalyazo().getMerkozesekSzama();
			return osszeg>getPALYAZATI_OSSZEG_HATAR()?getPALYAZATI_OSSZEG_HATAR():osszeg;
		} else {
			return 0;
		}
	}

	public static int getMERKOZESENKENTI_DIJ() {
		return MERKOZESENKENTI_DIJ;
	}

	public static void setMERKOZESENKENTI_DIJ(int mERKOZESENKENTI_DIJ) {
		MERKOZESENKENTI_DIJ = mERKOZESENKENTI_DIJ;
	}

}
