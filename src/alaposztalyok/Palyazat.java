package alaposztalyok;

import java.util.Random;

public class Palyazat {

	private static int PALYAZATI_OSSZEG_HATAR;

	private static int egyediSorszam;

	private int sorszam;

	private Palyazo palyazo;

	public Palyazat(Palyazo palyazo) {
		this.palyazo = palyazo;
		egyediSorszam++;
		this.sorszam = egyediSorszam ;
	}

	/**
	 * @return Ha az osszeghat�r felett gener�l sz�mot akkor 0-t ad vissza.
	 */
	public int folyosithatoOsszeg() {

		Random rand = new Random();
		int osszeg = rand.nextInt(PALYAZATI_OSSZEG_HATAR);

		if (osszeg >= PALYAZATI_OSSZEG_HATAR) {
			return 0;
		} else {
			return osszeg;
		}
	}

	public Palyazo getPalyazo() {
		return palyazo;
	}

	public int getSorszam() {
		return sorszam;
	}

	public static int getPALYAZATI_OSSZEG_HATAR() {
		return PALYAZATI_OSSZEG_HATAR;
	}

	public static void setPALYAZATI_OSSZEG_HATAR(int pALYAZATI_OSSZEG_HATAR) {
		PALYAZATI_OSSZEG_HATAR = pALYAZATI_OSSZEG_HATAR;
	}

	@Override
	public String toString() {
		return (this.sorszam + ". " + this.palyazo.toString());
	}
}
