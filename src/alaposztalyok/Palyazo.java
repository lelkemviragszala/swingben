package alaposztalyok;

public class Palyazo{

	private String nev;

    private String kod;

	private int okossagiSzint;

	private boolean tanariAjanlas;

	private int merkozesekSzama;

	private boolean edzoiAjanlas;

	private static int MERKOZES_SZAM_HATAR;

	// e f�l�tt j�r edz�i aj�nl�s
	private static int OKOSSAG_HATAR;

	public Palyazo(String nev, String kod) {
		this.nev = nev;
		this.kod = kod;
		okossagiSzint=0;
        merkozesekSzama=0;

	}

	public void tanul(){
		okossagiSzint++;
		if(okossagiSzint==OKOSSAG_HATAR){
			tanariAjanlas=true;
		}
	}

	public void sportol(){
		merkozesekSzama++;
		if(merkozesekSzama==MERKOZES_SZAM_HATAR){
			edzoiAjanlas=true;
		}
	}

    public static int getMERKOZES_SZAM_HATAR() {
		return MERKOZES_SZAM_HATAR;
	}

	public String getNev() {
	    return nev;
	}

	public int getOkossagiSzint() {
	    return okossagiSzint;
	}

	public int getMerkozesekSzama() {
	    return merkozesekSzama;
	}

	public String getKod() {
	    return kod;
	}

	public static int getOKOSSAG_HATAR() {
		return OKOSSAG_HATAR;
	}

	public boolean isTanariAjanlas() {
	    return tanariAjanlas;
	}

	public boolean isEdzoiAjanlas() {
	    return edzoiAjanlas;
	}

	public static void setMERKOZES_SZAM_HATAR(int mERKOZES_SZAM_HATAR) {
		MERKOZES_SZAM_HATAR = mERKOZES_SZAM_HATAR;
	}

	public static void setOKOSSAG_HATAR(int oKOSSAG_HATAR) {
		OKOSSAG_HATAR = oKOSSAG_HATAR;
	}

	@Override
	public String toString() {
	    return (this.getNev()+" ("+this.getKod()+")"); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return obj.toString().equals(this.toString());
	}





}
